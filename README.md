# Patis X Clima 2023

A  WordPress theme for the _Patis X Clima_ project web site. Based up on on [GeneratePress](https://generatepress.com/) a GPL theme [developed here](https://github.com/tomusborne/generatepress).

## Description
This repository contains the files in order to view the _Patis x Clima_ with its colors, design and needs.

## Installation
Clone the repository and place the theme files into the Wordpress themes directory, and the plugin directory inside Plugins.

## Support
If you find anyu problem or have any opinion about it you can fill an [Issue](https://gitlab.com/jorge-vitrubio/patisxclima2023-gpchild/-/issues)


## Authors and acknowledgment
This theme would not have been possible without Automatic [_s](https://github.com/automattic/_s) theme.

## License
[GNU General Public License v2.0](https://gitlab.com/jorge-vitrubio/patisxclima2023/-/blob/main/LICENSE)

## Project status
Always under development.

## Graphics

### Colors
 - patisxclima-color-01: css `var(--accent)`     hex `#424a24` rgb `rgb(66,74,36)`
 - patisxclima-color-02: css `var(--contrast-2)` hex `#5b6e33` rgb `rgb(91,110,51)`
 - patisxclima-color-03: css `var(--contrast-3)` hex `#758f3a` rgb `rgb(117,143,58)`
 - patisxclima-color-04: css `var(--base)`       hex `#94a738` rgb `rgb(148,167,56)`
 - patisxclima-color-05: css `var(--base-2)`     hex `#c4cd86` rgb `rgb(196,205,134)`
 - patisxclima-color-06: css `var(--base-3)`     hex `#d0d7a0` rgb `rgb(208,215,160)`
 - patisxclima-color-07: css `var(--bkgcolor)`   hex `#f4f3d8` rgb `rgb(244,243,216)` 
 - whiter clear color:   css `var(--whitecolor)` hex `#fefefe` rgb `rgb(254,254,254)` 
 - darker black color:   css `var(--contrast)`   hex `#333333` rgb `rgb(51,51,51)`

```
:root {
  --accent:     rgb(66,74,36)   ; /*#424a24*/
  --contrast:   rgb(51,51,51)   ; /*#333333*/
  --contrast-2: rgb(91,110,51)  ; /*#5b6e33*/
  --contrast-3: rgb(117,143,58) ; /*#758f3a*/
  --base:       rgb(148,167,56) ; /*#94a738*/
  --base-2:     rgb(196,205,134); /*#c4cd86*/
  --base-3:     rgb(208,215,160); /*#d0d7a0*/
  --bkgcolor:   rgb(244,243,216); /*#f4f3d8*/
  --whitecolor:      rgb(254,254,254); /*#fefefe*/
}
```
for the "en-accio" or "en-accion" category slugs the colors are:

- accent     hex `#ff6600`
- contrast   hex `#ff7711`
- contrast-2 hex `#ff8822`
- contrast-3 hex `#ff9933`
- base       hex `#ffaa44`
- base-2     hex `#ffbb55`
- base-3     hex `#ffcc66`

```
    [class*="-en-accio"] .site-main ,
    .site [class*="-en-accio"]  {                                                                       --accent:      #ff6600;
        --contrast:    #ff7711;
        --contrast-2:  #ff8822;
        --contrast-3: #ff9933;
        --base:    #ffaa44;
        --base-2:  #ffbb55;
        --base-3:  #ffcc66;
    }
```

functions.php calls -> inc/defaults.php -> colors:
```
'global_colors' => array(
	array(
		'name' => __( 'Accent', 'generatepress' ),
		'slug' => 'accent',
		'color' => '#424a24',
	),
	array(
		'name' => __( 'Contrast', 'generatepress' ),
		'slug' => 'contrast',
		'color' => '#333333',
	),
	array(
		/* translators: Contrast number */
		'name' => sprintf( __( 'Contrast %s', 'generatepress' ), '2' ),
		'slug' => 'contrast-2',
		'color' => '#5b6e33',
	),
	array(
		/* translators: Contrast number */
		'name' => sprintf( __( 'Contrast %s', 'generatepress' ), '3' ),
		'slug' => 'contrast-3',
		'color' => '#758f3a',
	),
	array(
		'name' => __( 'Base', 'generatepress' ),
		'slug' => 'base',
		'color' => '#94a738',
	),
	array(
		/* translators: Base number */
		'name' => sprintf( __( 'Base %s', 'generatepress' ), '2' ),
		'slug' => 'base-2',
		'color' => '#c4cd86',
	),
	array(
		/* translators: Base number */
		'name' => sprintf( __( 'Base %s', 'generatepress' ), '3' ),
		'slug' => 'base-3',
		'color' => '#d0d7a0',
	),
	array(
		'name' => __( 'Background color', 'generatepress' ),
		'slug' => 'bkgcolor',
		'color' => '#f4f3d8',
	),
	array(
		'name' => __( 'White', 'generatepress' ),
		'slug' => 'whitecolor',
		'color' => '#fefefe',
	),
)
```

## Header

### Header Claim
add this to "top bar"
```html
<h6>architecture<em>for</em><em>by</em><em>with</em>people<h6>
```

with this css will do a carousel effect
```css
    .top-bar .wp-block-heading {
        margin: 0;
        text-transform: lowercase;
        color: var(--contrast-2);
        font-family: var(--font-pxc);
        font-weight: normal;
        font-size: calc( var(--base-font-size) * 1.1);
    }
    .top-bar .wp-block-heading em {
        width:0;
        margin: 0 auto;
        color: var(--contrast-2);
        display: inline-block;
        text-align: center;
        opacity: 0;
        animation-duration: 6s;
        animation-delay: -6s;
        animation-iteration-count: infinite;
    }
    .top-bar .wp-block-heading em:nth-child(1) {
        animation-name: claim01FadeInOut;

    }
    .top-bar .wp-block-heading em:nth-child(2) {
        animation-name: claim02FadeInOut;
    }
    .top-bar .wp-block-heading em:nth-child(3) {
        animation-name: claim03FadeInOut;
    }
    @-moz-keyframes claimFadeInOut {
        0%,100% { opacity: 0 }
        50% { opacity: 1 }
    }
    @-webkit-keyframes claimFadeInOut {
        0%,100% { opacity: 0 }
        50% { opacity: 1 }
    }
    @keyframes claim01FadeInOut {
        0%,33% { opacity: 1; width: auto; min-width: 2em; }
        34%,100% { opacity: 0; width: 0px; min-width: 0; }
    }
    @keyframes claim02FadeInOut {
        0%,33% { opacity: 0;  width: 0px; min-width: 0; }
        34%,66% { opacity: 1; width: auto; min-width: 2em; }
        67%,100% { opacity: 0; width: 0px; min-width: 0; }
    }
    @keyframes claim03FadeInOut {
        0%,66% { opacity: 0;  width: 0px; min-width: 0; }
        67%,100% { opacity: 1; width: auto; min-width: 2em; }
    }
```
