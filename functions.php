<?php
/**
 * PatisXClima GP child theme functions and definitions.
 *
 * Add your custom PHP in this file.
 * Only edit this file if you have direct access to it on your server (to fix errors if they happen).
 */

/* modify defaults  */
include get_stylesheet_directory() . '/inc/defaults.php';

/* remove generate press dashboard */
include get_stylesheet_directory() . '/inc/remove-generatepress-dashboard.php';

/* add styles for blocks editor */
include get_stylesheet_directory() . '/inc/editor-styles.php';

/* add styles for login */
include get_stylesheet_directory() . '/inc/login-my-logo.php';

/* add gallery defaul link to media */
include get_stylesheet_directory() . '/inc/gallery-link-media.php';
