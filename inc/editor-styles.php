<?php
/**
 * https://docs.generatepress.com/
 * https://docs.generatepress.com/article/adding-local-fonts/#show-local-fonts-in-wp-block-editor
 *
 * show fonts in editor style, blocks, etc..
 *
 * @package PatisXClima-gpchild
 */
add_filter( 'generate_editor_styles', function( $editor_styles ) {
    $editor_styles[] = 'style.css';

    return $editor_styles;
} );
