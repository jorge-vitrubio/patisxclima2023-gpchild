<?php
/**
 * Overwrite gallery short code
 *
 * Link images to media file in all galleries 
 *
 * https://stackoverflow.com/a/21798246
 *
 * @package PatisXClima-gpchild
 */

add_shortcode( 'gallery', 'pxc_gallery_shortcode' );
  function pxc_gallery_shortcode( $atts ) {
      $atts['link'] = 'file';
      return gallery_shortcode( $atts );
  }
