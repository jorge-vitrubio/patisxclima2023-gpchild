<?php
/**
 * My own logo in the loggin window
 *
 * @package TheGreenBalloon-FP
 * @since TheGreenBalloon-FP 1.0.0
 */

 // customize the loggin
 // https://codex.wordpress.org/Customizing_the_Login_Form
 // 

if ( ! function_exists( 'my_login_logo' ) ) :
	function my_login_logo() {
		$custom_logo_id = get_theme_mod( 'custom_logo' );
		$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
		?>
	    <style type="text/css">
	        #login h1 a, .login h1 a {
	            background-image: url('<?php 	echo $image[0];?>');
						}
	    </style>
	<?php }
	add_action( 'login_enqueue_scripts', 'my_login_logo' );
endif;
if ( ! function_exists( 'my_login_logo_url' ) ) :
	function my_login_logo_url() {
	    return home_url();
	}
	add_filter( 'login_headerurl', 'my_login_logo_url' );
endif;
if ( ! function_exists( 'my_login_logo_url_title' ) ) :
	function my_login_logo_url_title() {
	    return 'Your Site Name and Info';
	}
	add_filter( 'login_headertitle', 'my_login_logo_url_title' );
endif;
if ( ! function_exists( 'my_login_stylesheet' ) ) :
	function my_login_stylesheet() {
	    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/style.css' );
//	    wp_enqueue_script( 'custom-login', get_stylesheet_directory_uri() . '/style-login.js' );
	}
	add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );
endif;
