<?php                                                                                                            
/**
 * remove generatepress dashboard for non admins
 * check capabilities at
 * https://wordpress.org/documentation/article/roles-and-capabilities/
 *
 * @package PatisXClima-gpchild
 */


add_filter( 'generate_dashboard_page_capability', 'pxc23_super_admin_dashboard' );
function pxc23_super_admin_dashboard() {
  //return 'manage_sites'; //superadmin role
  return 'customize'; //admin role
  //return 'edit_others_pages'; //edit role
}
