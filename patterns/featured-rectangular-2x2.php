<!-- wp:group {"align":"wide","className":"featured-special rectangles-2","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide featured-special rectangles-2" id="destacats-especials"><!-- wp:columns {"verticalAlignment":"bottom"} -->
<div class="wp-block-columns are-vertically-aligned-bottom"><!-- wp:column {"verticalAlignment":"bottom","width":"50%"} -->
<div class="wp-block-column is-vertically-aligned-bottom" style="flex-basis:50%"><!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">voleu renaturalizar l'escola?</h4>
<!-- /wp:heading -->

<!-- wp:cover {"url":"http://localhost:8000/wp-content/uploads/2023/12/nens-nenes-pati-ort-scaled.jpg","id":7853,"dimRatio":0,"overlayColor":"whitecolor","focalPoint":{"x":0.45,"y":0.54},"contentPosition":"center right","isDark":false,"layout":{"type":"constrained"}} -->
<div class="wp-block-cover is-light has-custom-content-position is-position-center-right"><span aria-hidden="true" class="wp-block-cover__background has-whitecolor-background-color has-background-dim-0 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-7853" alt="" src="http://localhost:8000/wp-content/uploads/2023/12/nens-nenes-pati-ort-scaled.jpg" style="object-position:45% 54%" data-object-fit="cover" data-object-position="45% 54%"/><div class="wp-block-cover__inner-container"><!-- wp:group {"backgroundColor":"bkgcolor","textColor":"accent","className":"square","layout":{"type":"constrained"}} -->
<div class="wp-block-group square has-accent-color has-bkgcolor-background-color has-text-color has-background"><!-- wp:heading {"textAlign":"left","level":3} -->
<h3 class="wp-block-heading has-text-align-left">Manual de<br>renaturalizació<br>d'espais educatius</h3>
<!-- /wp:heading -->

<!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button {"className":"is-style-outline"} -->
<div class="wp-block-button is-style-outline"><a class="wp-block-button__link wp-element-button">descarregar aquí</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"bottom","width":"50%"} -->
<div class="wp-block-column is-vertically-aligned-bottom" style="flex-basis:50%"><!-- wp:heading {"textAlign":"right","level":4} -->
<h4 class="wp-block-heading has-text-align-right">i després de la renaturalització... que?</h4>
<!-- /wp:heading -->

<!-- wp:cover {"url":"http://localhost:8000/wp-content/uploads/2023/09/230903_inici-pxcA.jpg","id":7109,"dimRatio":0,"overlayColor":"whitecolor","contentPosition":"center right","isDark":false,"layout":{"type":"constrained"}} -->
<div class="wp-block-cover is-light has-custom-content-position is-position-center-right"><span aria-hidden="true" class="wp-block-cover__background has-whitecolor-background-color has-background-dim-0 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-7109" alt="" src="http://localhost:8000/wp-content/uploads/2023/09/230903_inici-pxcA.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"backgroundColor":"base-2","textColor":"accent","className":"square","layout":{"type":"constrained"}} -->
<div class="wp-block-group square has-accent-color has-base-2-background-color has-text-color has-background"><!-- wp:heading {"textAlign":"left","level":3} -->
<h3 class="wp-block-heading has-text-align-left">Patis x clima<br>en acció</h3>
<!-- /wp:heading -->

<!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button {"className":"is-style-outline"} -->
<div class="wp-block-button is-style-outline"><a class="wp-block-button__link wp-element-button">+ info</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns {"verticalAlignment":"bottom"} -->
<div class="wp-block-columns are-vertically-aligned-bottom"><!-- wp:column {"verticalAlignment":"bottom","width":"50%"} -->
<div class="wp-block-column is-vertically-aligned-bottom" style="flex-basis:50%"><!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">renaturalització al territori</h4>
<!-- /wp:heading -->

<!-- wp:cover {"url":"http://localhost:8000/wp-content/uploads/2020/10/mapa-PxC_1000x400.jpg","id":4190,"dimRatio":0,"overlayColor":"whitecolor","contentPosition":"center right","isDark":false,"layout":{"type":"constrained"}} -->
<div class="wp-block-cover is-light has-custom-content-position is-position-center-right"><span aria-hidden="true" class="wp-block-cover__background has-whitecolor-background-color has-background-dim-0 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-4190" alt="" src="http://localhost:8000/wp-content/uploads/2020/10/mapa-PxC_1000x400.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"backgroundColor":"contrast-3","textColor":"accent","className":"square","layout":{"type":"constrained"}} -->
<div class="wp-block-group square has-accent-color has-contrast-3-background-color has-text-color has-background"><!-- wp:heading {"textAlign":"left","level":3} -->
<h3 class="wp-block-heading has-text-align-left">Mapa déspais <br>educatius <br>renaturalizats</h3>
<!-- /wp:heading -->

<!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button {"className":"is-style-outline"} -->
<div class="wp-block-button is-style-outline"><a class="wp-block-button__link wp-element-button">veure +</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"bottom","width":"50%"} -->
<div class="wp-block-column is-vertically-aligned-bottom" style="flex-basis:50%"><!-- wp:heading {"textAlign":"right","level":4} -->
<h4 class="wp-block-heading has-text-align-right">jornades de renaturalització<br>d’espais i entorns edudatius</h4>
<!-- /wp:heading -->

<!-- wp:cover {"url":"http://localhost:8000/wp-content/uploads/2023/11/231018_VII-jornades-patis-x-clima-tarda-5-scaled.jpeg","id":7354,"dimRatio":0,"overlayColor":"whitecolor","focalPoint":{"x":0.48,"y":0.49},"contentPosition":"center right","layout":{"type":"constrained"}} -->
<div class="wp-block-cover has-custom-content-position is-position-center-right"><span aria-hidden="true" class="wp-block-cover__background has-whitecolor-background-color has-background-dim-0 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-7354" alt="" src="http://localhost:8000/wp-content/uploads/2023/11/231018_VII-jornades-patis-x-clima-tarda-5-scaled.jpeg" style="object-position:48% 49%" data-object-fit="cover" data-object-position="48% 49%"/><div class="wp-block-cover__inner-container"><!-- wp:group {"backgroundColor":"bkgcolor","textColor":"accent","className":"square","layout":{"type":"constrained"}} -->
<div class="wp-block-group square has-accent-color has-bkgcolor-background-color has-text-color has-background"><!-- wp:heading {"textAlign":"left","level":3} -->
<h3 class="wp-block-heading has-text-align-left">Jornades de <br>renaturalització <br>déspais i entorns <br>educatius</h3>
<!-- /wp:heading -->

<!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button {"className":"is-style-outline"} -->
<div class="wp-block-button is-style-outline"><a class="wp-block-button__link wp-element-button">+ info</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->
